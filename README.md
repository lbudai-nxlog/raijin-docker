# raijin-docker

### build docker image

```
./build-image.sh
```

### start the container 

```
./start-container.sh
```

After starting the container, the raijin server is accessible at `localhost:2500` (port is exposed).


### newer raijin installer
just remove the wget line from the Dockerfile , download your installer, eg.:
`./installer/raijin-server_latest_greatest_version_amd64.deb`

```
ADD installer/raijin-server_latest_greatest_version_amd64.deb raijin-server_latest_greatest_version_amd64.deb
```

then modify the `dpkg` line:
```
RUN dpkg -i raijin-server_latest_greatest_version_amd64.deb
```


