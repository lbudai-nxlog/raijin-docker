FROM debian:buster

RUN apt-get update -qq && apt-get install -y wget lsb-base
RUN wget http://raijindb.com/system/files/products/files/1/raijin-server_0.9.2363_amd64.deb
RUN dpkg -i raijin-server_0.9.2363_amd64.deb
EXPOSE 2500/tcp

ADD start_raijin_daemon.sh start_raijin_daemon.sh

ENTRYPOINT ["./start_raijin_daemon.sh"]


